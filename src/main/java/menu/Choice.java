package menu;

public enum Choice {
    INFO("Get info"),
    EXIT("Exit"),
    PRINT("Print input text"),
    MAIN("Main method");

    private final  String offer;


    Choice(String offer) {
        this.offer = offer;
    }
    public String toString() {
        return this.offer;
    }
}
