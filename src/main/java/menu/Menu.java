package menu;

import java.util.*;

public class Menu {
    private Map<Integer, Choice> showMenu = new HashMap();
    private Map<Integer, Method> doingSomething = new LinkedHashMap<Integer, Method>();

    public Menu() {
     view();
    }

    private void view() {
        showMenu.put(1, Choice.INFO);
        showMenu.put(2, Choice.MAIN);
        showMenu.put(3, Choice.PRINT);
        showMenu.put(4, Choice.EXIT);
        doingSomething.put(1, this::info);
        doingSomething.put(2, this::main);
        doingSomething.put(3, this::print);
        doingSomething.put(4, this::exit);
    }

    void print() {
        System.out.println("print");
    }

    void main() {
        System.out.println("main");
    }

    void info() {
        System.out.println("info");
    }

    void exit() {
        System.exit(0);
    }

    public void workMenu() {
        showMenu();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Please select point.");
            try {
                Integer choice = scanner.nextInt();
                doingSomething.get(choice).doing();
                System.out.println();
            } catch (Exception e) {

                System.out.println("Error, please try again");

                e.printStackTrace();
            }
        }
    }

    public void showMenu() {
        for (Integer key: showMenu.keySet()) {

            System.out.println( key + " "+ showMenu.get(key).toString());
        }
    }
}




